« Inspecteur Nao » était initialement une application où le robot interrogeait des suspects dans le but de reconstituer une enquête. Le but a été sensiblement revu afin que la jouabilité et le ressenti utilisateur soient plus agréables. L'application a donc été renommée « Commissaire Nao ».

Le Commissaire robotique en face de vous dicte les règles du jeu : vous voilà inspecteur sous ses ordres. Vous allez devoir être très attentif à ce qu'il vous dira et effectuer les quêtes qu'ils vous confiera, puis rapporter les bonnes informations afin de faire évoluer le scénario.

Ce programme s'apparente à un jeu où vous êtes le héros ou à un jeu de rôle. Le robot est alors le maître du jeu (le « PNJ », Personnage Non Joueur). Ce concept de jeu renverse le concept de la plupart des jeux que nous avions pu voir. Il permet entre autre de tester de nombreuses capacités de la plateforme telles que la reconnaissance auditive/faciale, la gestuelle, la mémoire, qui ont toutes attrait à l'interraction avec l'Homme.

Nous espérons sincèrement que ce programme vous permettra de vous amuser tout en ayant une nouvelle perspective des possibilités du robot Nao.

Créateurs de l'œuvre et de sa documentation : Charlotte GAILLEDREAU, Vincent LAMBERT, MacCall, Sébastien MARTY, Guillaume MELLET, Charlotte RUNEL.

Licence de l'œuvre et de sa documentation : Paternité + Partage dans les mêmes conditions (BY SA) : Les titulaires des droits, Charlotte GAILLEDREAU , Vincent LAMBERT, MacCall, Sébastien MARTY , Guillaume MELLET , Charlotte RUNEL, autorisent toute utilisation de l’œuvre originale (y compris à des fins commerciales) ainsi que la création d’œuvres dérivées, à condition qu’elles soient distribuées sous une licence identique à celle qui régit l’œuvre originale. 
http://creativecommons.org/licenses/by/3.0/fr/

Un grand merci à toute l'équipe du Carrefour Numérique², du LivingLab, du FabLab – et particulièrement à David pour son aide et sa patience !
